const router = require("express").Router();
const checkAuth = require("../utils/checkAuth.js");
const UserController = require("../controllers/UserController.js");
router.post('/auth/login', UserController.login);

router.post('/auth/register', UserController.register);
router.get('/auth/me/:id', checkAuth, UserController.getMe);
router.delete('/user', checkAuth, UserController.remove);

module.exports = router