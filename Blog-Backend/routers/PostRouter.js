const router = require("express").Router();
const PostController = require("../controllers/PostController.js");
const checkAuth = require("../utils/checkAuth.js");

router.get('/tags', PostController.getLastTags);

router.get('/posts', PostController.getAll);
router.get('/posts/tags', PostController.getLastTags);
router.get('/posts/:id', PostController.getOne);
router.post('/posts', checkAuth, PostController.create);
router.delete('/posts/:id', checkAuth, PostController.remove);
router.patch(
    '/posts/:id',
    checkAuth,
    PostController.update,
);


module.exports = router