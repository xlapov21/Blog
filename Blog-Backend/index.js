const express = require('express');
const cors = require('cors');
const mongoose  = require('mongoose');
const postRouter = require('./routers/PostRouter.js')
const userRouter = require('./routers/UserRouter.js')

const app = express();


app.use(express.json());
app.use(cors());
app.use('/uploads', express.static('uploads'));
app.use(postRouter)
app.use(userRouter)

const start = async () => {
  try {
    mongoose
        .connect('mongodb+srv://Kuzy:wwwwww@cloud.npgxmkr.mongodb.net/blog?retryWrites=true&w=majority')
        .then(() => console.log('DB ok'))
        .catch((err) => console.log('DB error', err));

    app.listen(4000);
    console.log('Server OK');
  } catch (e) {
    console.error(e);
  }
};

module.exports = app;
start();