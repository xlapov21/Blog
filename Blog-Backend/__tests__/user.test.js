const request = require("supertest")
const app = require("../index.js")
const bcrypt = require("bcrypt");
const UserModel = require("../models/User");

let newUser = {
    email: '10a@gmail.com',
    fullName: 'privet ya test',
    password: 'password',
};

let newUser2 = {
    email: '100a@gmail.com',
    fullName: 'privet ya test',
    password: 'password',
};

const UserAlreadyExists = {
    email: '10a@gmail.com',
    fullName: 'privet ya test',
    password: 'password',
};

const nonExistUser = {
    email: '100000a@gmail.com',
    fullName: 'privet ya test',
    password: 'password',
};

const wrongUser = {
    email: '10a@gmail.com',
    fullName: 'privet ya test',
    password: 'wrongPassword',
};

// beforeAll(async () => {
//     jest.setTimeout(10000);
// });

beforeAll(async () => {
    await UserModel.deleteMany({ fullName: newUser.fullName });
});

beforeAll(async () => {
    await UserModel.deleteMany({ fullName: newUser2.fullName });
});

beforeAll(async () => {
    jest.setTimeout(3000);
});


describe("Login/Register", () => {
    it("it should not be created, since there is such a user", async () => {
        const response = await request(app)
            .post("/auth/register")
            .send(UserAlreadyExists)

        expect(response.status).toBe(500);
        expect(response.body.error).toBe('Не удалось зарегистрироваться');
    });

    it("should register new user with valid credentials", async () => {
        const response = await request(app)
            .post("/auth/register")
            .send(newUser2)


        expect(response.status).toBe(200);
        expect(response.body.fullName).toBe(newUser2.fullName);
    });

    it("should not log in because user is non exist", async () => {
        const response = await request(app)
            .post("/auth/login")
            .send(nonExistUser)


        expect(response.status).toBe(404);
        expect(response.body.error).toBe('Пользователь не найден');
    });

    it("getMe failure - not auth", async () => {
        const response = await request(app)
            .get(`/auth/me/652952f3b447af3e14260ac7`)


        expect(response.status).toBe(403);
        expect(response.body.error).toBe('Нет доступа');
    });

    it("getMe failure - not found user", async () => {
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const response = await request(app)
            .get(`/auth/me/652952f3b447af3e14260ac7`)
            .set('Authorization', adminToken)

        expect(response.status).toBe(404);
        expect(response.body.error).toBe('Пользователь не найден');
    });

    it("delete user failure - not auth", async () => {
        const response = await request(app)
            .delete("/user")
            .send({email: newUser.email})

        expect(response.status).toBe(403);
        expect(response.body.error).toBe('Нет доступа');
    });

    it("delete user failure - non body", async () => {
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const response = await request(app)
            .delete("/user")
            .set('Authorization', adminToken)

        expect(response.status).toBe(500);
    });
});

beforeAll(async () => {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(newUser.password, salt);

    const _newUser = new UserModel({
        email: newUser.email,
        fullName: newUser.fullName,
        passwordHash: hash,
    });

    await _newUser.save();
})

beforeAll(async () => {
    jest.setTimeout(3000);
});

describe("Login/Register with User", () => {
    it("delete user failure - non valid email", async () => {
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const response = await request(app)
            .delete("/user")
            .set('Authorization', adminToken)
            .send({email: nonExistUser.email})

        expect(response.status).toBe(404);
        expect(response.body.error).toBe('Юзер не найден');
    });

    it("delete user success", async () => {
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const response = await request(app)
            .delete("/user")
            .set('Authorization', adminToken)
            .send({email: newUser.email})

        expect(response.status).toBe(200);
    });

    it("getMe success", async () => {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(newUser.password, salt);

        const _newUser = new UserModel({
            email: newUser.email,
            fullName: newUser.fullName,
            passwordHash: hash,
        });

        await _newUser.save();
        const user = await UserModel.findOne({email: newUser.email});
        const userId = user?._id
        console.log('==========>user', user)
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const response = await request(app)
            .get(`/auth/me/${userId}`)
            .set('Authorization', adminToken)

        expect(response.status).toBe(200);
    });

    it("getMe failed - not valid auth", async () => {
        const user = await UserModel.findOne({email: newUser.email});
        const userId = user?._id

        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ayJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const response = await request(app)
            .get(`/auth/me/${userId}`)
            .set('Authorization', adminToken)

        expect(response.status).toBe(403);
        expect(response.body.error).toBe('Нет доступа');
    });

    it("should log in", async () => {
        const response = await request(app)
            .post("/auth/login")
            .send(newUser)

        expect(response.status).toBe(200);
    });


    it("should not log in because the credentials is incorrect", async () => {
        const response = await request(app)
            .post("/auth/login")
            .send(wrongUser)

        expect(response.status).toBe(400);
        expect(response.body.error).toBe('Неверный логин или пароль');
    });

})