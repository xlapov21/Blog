const request = require("supertest")
const app = require("../index.js")
const PostModel = require("../models/Post")
const UserModel = require("../models/User");
const bcrypt = require("bcrypt");

const newPost = {
    title: 'this is a new post title',
    text: 'this is a new post content',
}

const updatePost = {
    title: 'this is a new post title UPDATE',
    text: 'this is a new post content UPDATE',
}

const userForTestingPosts = {
    email: 'test11posts@gmail.com',
    fullName: 'privet ya test for posts',
    password: 'password',
};


// before(function () {
//     this.timeout(3000);
//     setTimeout(done, 2000);
// });


// beforeAll(async () => {
//     jest.setTimeout(3000);
// });

beforeAll(async () => {
    await UserModel.deleteMany({ fullName: userForTestingPosts.fullName });
});


beforeAll(async () => {
    await PostModel.deleteMany({ text: newPost.text });
});

describe("Posts", () => {
    it("getLastTags success", async () => {
        const response = await request(app)
            .get("/posts/tags")

        expect(response.status).toBe(200);
    });

    it("getAll success", async () => {
        const response = await request(app)
            .get("/posts")

        expect(response.status).toBe(200);
    });

    it("getOne failed - not found post", async () => {
        const nonExistPostId = '650e20a026237bc06031cb23'
        const response = await request(app)
            .get(`/posts/${nonExistPostId}`)

        expect(response.status).toBe(404);
        expect(response.body.error).toBe('Статья не найдена');
    });

    it("getOne success", async () => {
        const existPostId = '650e20a026237bc06031cb22'
        await request(app)
            .get(`/posts/${existPostId}`)
            .expect(200)
    });
});

beforeAll(async () => {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(userForTestingPosts.password, salt);

    const newUser = new UserModel({
        email: userForTestingPosts.email,
        fullName: userForTestingPosts.fullName,
        passwordHash: hash,
    });

    await newUser.save();
})

beforeAll(async () => {
    jest.setTimeout(3000);
});

describe("Posts with User", () => {
    it("create post failure - not auth", async () => {
        const nonExistUserId = 24235423532
        const response = await request(app)
            .post("/posts")
            .send({...newPost, userId: nonExistUserId})

        expect(response.status).toBe(403);
        expect(response.body.error).toBe("Нет доступа");
    });

    it("create post success", async () => {
        const user = await UserModel.findOne({email: userForTestingPosts.email});
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const responsePost = await request(app)
            .post("/posts")
            .set('Authorization', adminToken)
            .send({...newPost, userId: String(user._id)})
        expect(responsePost.status).toBe(201);

    });

    it("update post failure - not auth", async () => {
        const post = await PostModel.findOne({text: newPost.text});
        const postId = post?._id

        const responseUpdatePost = await request(app)
            .patch(`/posts/${postId}`)
            .send({
                title: updatePost.title,
                text: updatePost.text,
            })

        expect(responseUpdatePost.status).toBe(403);
        expect(responseUpdatePost.body.error).toBe("Нет доступа");
    });

    it("update post failure - not postId", async () => {
        const postId = undefined
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const responseUpdatePost = await request(app)
            .patch(`/posts/${postId}`)
            .set('Authorization', adminToken)
            .send({
                title: updatePost.title,
                text: updatePost.text,
            })

        expect(responseUpdatePost.status).toBe(500);
        expect(responseUpdatePost.body.error).toBe("Не удалось обновить статью");
    });

    it("update post success", async () => {
        const user = await UserModel.findOne({email: userForTestingPosts.email});
        const userId = user?._id

        const post = await PostModel.findOne({text: newPost.text});
        const postId = post?._id

        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const responseUpdatePost = await request(app)
            .patch(`/posts/${postId}`)
            .set('Authorization', adminToken)
            .send({
                title: updatePost.title,
                text: updatePost.text,
                user: userId,
            })

        expect(responseUpdatePost.status).toBe(200);
        expect(responseUpdatePost.body.success).toBe(true);
    });

    it("delete post failure - non exist postId", async () => {
        const user = await UserModel.findOne({email: userForTestingPosts.email});

        const nonExistPostId = '150e20a026237bc06031cb22'
        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const responsePost = await request(app)
            .delete(`/posts/${nonExistPostId}`)
            .set('Authorization', adminToken)
            .send({userId: String(user._id)})

        expect(responsePost.status).toBe(404);
        expect(responsePost.body.error).toBe("Статья не найдена");
    });

    it("delete post success", async () => {
        const post = await PostModel.findOne({text: updatePost.text});
        const postId = post?._id

        const adminToken =
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6InBhc3N3b3JkIn0.CMGzoyDyNo742a3Ks6S-1CExjZ6nZTZAiYRqioQ2iZo';

        const responsePost = await request(app)
            .delete(`/posts/${postId}`)
            .set('Authorization', adminToken)

        expect(responsePost.status).toBe(200);
        expect(responsePost.body.success).toBe(true);
    });
});