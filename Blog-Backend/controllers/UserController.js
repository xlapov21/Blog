const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const UserModel = require('../models/User.js');
const PostModel = require("../models/Post");

class UserController {
  async register(req, res) {
      try {
        const password = req.body.password;
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);

        const doc = new UserModel({
          email: req.body.email,
          fullName: req.body.fullName,
          avatarUrl: req.body.avatarUrl,
          passwordHash: hash,
        });

        const user = await doc.save();

        const token = jwt.sign(
            {
              _id: user._id,
            },
            'secret123',
            {
              expiresIn: '30d',
            },
        );
        
        const {passwordHash, ...userData} = user._doc;

        res.json({
          ...userData,
          token,
        });
      } catch (err) {
        res.status(500).json({
          error: 'Не удалось зарегистрироваться',
        });
      }
    };

  async login(req, res) {
      try {
        const user = await UserModel.findOne({email: req.body.email});

        if (!user) {
          return res.status(404).json({
            error: 'Пользователь не найден',
          });
        }

        const isValidPass = await bcrypt.compare(req.body.password, user._doc.passwordHash);

        if (!isValidPass) {
          return res.status(400).json({
            error: 'Неверный логин или пароль',
          });
        }

        const token = jwt.sign(
            {
              _id: user._id,
            },
            'secret123',
            {
              expiresIn: '30d',
            },
        );

        const {passwordHash, ...userData} = user._doc;

        res.json({
          ...userData,
          token,
        });
      } catch (err) {
        res.status(500).json({
          error: 'Не удалось авторизоваться',
        });
      }
    };

  async getMe(req, res) {
      try {
        const user = await UserModel.findById(req.params.id);

        if (!user) {
          return res.status(404).json({
            error: 'Пользователь не найден',
          });
        }

        const {passwordHash, ...userData} = user._doc;

        res.json(userData);
      } catch (err) {
        console.log(err, 'err')
        res.status(500).json({
          error: 'Нет доступа',
        });
      }
    };

  async remove(req, res) {
    try {
      if (!req.body.email) {
        throw new Error()
      }
      UserModel.findOneAndDelete(
          {
            email: req.body.email,
          },
          (err, doc) => {
            if (!doc) {
              return res.status(404).json({
                error: 'Юзер не найден',
              });
            }

            res.status(200).json({
              success: true,
            });
          },
      );
    } catch (err) {
      res.status(500).json({
        error: 'Ошибка',
      });
    }
  };

}
module.exports = new UserController()

