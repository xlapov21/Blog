const PostModel = require('../models/Post.js');

class PostController {
  async getLastTags(req, res) {
    try {
      const posts = await PostModel.find().limit(5).exec();

      const tags = posts
          .map((obj) => obj.tags)
          .flat()
          .slice(0, 5);

      res.json(tags);
    } catch (err) {
      res.status(500).json({
        error: 'Не удалось получить тэги',
      });
    }
  };

  async getAll(req, res) {
    try {
      const posts = await PostModel.find().populate('user').exec();
      res.json(posts);
    } catch (err) {
      res.status(500).json({
        error: 'Не удалось получить статьи',
      });
    }
  };

  async getOne(req, res) {
    try {
      const postId = req.params.id;
      PostModel.findOneAndUpdate(
          {
            _id: postId,
          },
          {
            $inc: { viewsCount: 1 },
          },
          {
            returnDocument: 'after',
          },
          (err, doc) => {
            if (!doc) {
              return res.status(404).json({
                error: 'Статья не найдена',
              });
            }

            res.json(doc);
          },
      ).populate('user');
    } catch (err) {
      res.status(500).json({
        error: 'Не удалось получить статьи',
      });
    }
  };

  async remove(req, res) {
    try {
      const postId = req.params.id;
      PostModel.findOneAndDelete(
          {
            _id: postId,
          },
          (err, doc) => {
            if (!doc) {
              return res.status(404).json({
                error: 'Статья не найдена',
              });
            }

            res.status(200).json({
              success: true,
            });
          },
      );
    } catch (err) {
      res.status(500).json({
        error: 'Не удалось получить статьи',
      });
    }
  };

  async create(req, res) {
    try {
      const doc = new PostModel({
        title: req.body.title,
        text: req.body.text,
        imageUrl: req.body.imageUrl,
        tags: req.body.tags,
        user: req.body.userId,
      });

      const post = await doc.save();

      res.status(201).json(post);
    } catch (err) {
      res.status(500).json({
        error: 'Не удалось создать статью',
      });
    }
  };

  async update(req, res) {
    try {
      const postId = req.params.id;
      await PostModel.updateOne(
          {
            _id: postId,
          },
          {
            title: req.body.title,
            text: req.body.text,
            imageUrl: req.body.imageUrl,
            user: req.body.userId,
            tags: req.body?.tags?.split(','),
          },
      );

      res.status(200).json({
        success: true,
      });
    } catch (err) {
      res.status(500).json({
        error: 'Не удалось обновить статью',
      });
    }
  };
}

module.exports = new PostController()
